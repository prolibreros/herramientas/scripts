#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

# Instalación:
#   sudo wget https://gitlab.com/snippets/1917487/raw -P /usr/local/bin && sudo mv /usr/local/bin/raw /usr/local/bin/texti && sudo chmod +755 /usr/local/bin/texti

require 'fileutils'

begin
  # Cantidad de caracteres por línea de texto y aplicación a bloques por defecto
  chars = 60
  all   = false

  ARGV.each do |arg|
    if arg =~ /^-h$/ || arg =~ /^--help$/
      puts "texti ajusta el ancho de líneas de texto de un documento Markdown."
      puts "\nUso:"
      puts "  texti [argumentos] [archivo]"
      puts "\nArgumentos:"
      puts "  --chars=<número>  Cantidad de caracteres por línea de texto"
      puts "  --all             Corta cualquier tipo de bloque de Markdown"
      puts "  -h | --help       Muestra esta ayuda"
      puts "\nEjemplos:"
      puts "  texti archivo.md"
      puts "  texti --chars=100 archivo.md"
      puts "  texti --chars=100 --all archivo.md"
      puts "\nPor defecto una línea de texto equivale a 60 caracteres y el corte solo aplica a bloques de párrafos. Las líneas de texto se extienden hasta el fin de palabra."
      abort
    end

    # Cambia la cantidad de caracteres por línea de texto y detecta si se aplicará a todos los bloques
    if arg =~ /--chars=/
      chars = arg.gsub(/\D/,'').to_i
    elsif arg =~ /--all/
      all   = true
    end
  end

  # Analiza si es un archivo de Markdown
  if File.extname(ARGV.last) != '.md'
    puts "ERROR: solo archivos con extensión .md son permitidos, revisa su formato o ejecuta: texti --help."
    abort
  end

  # Crea un respaldo
  bak = File.basename(ARGV.last) + '.bak'
  puts "Creando respaldo en el mismo directorio con el nombre '#{bak}'…"
  FileUtils.cp(ARGV.last, File.dirname(ARGV.last) + '/' + bak)

  # Lee el archivo y divide por bloques
  content = File.read(ARGV.last).strip.split("\n\n")
  final   = []

  # Iteración para modificar cada una de las líneas
  content.each do |b|
    # Criterio a aplicar para el ajuste según si se quiere para todos los bloques o no
    if !all
      condition = b !~ /^#/        &&  # Encabezados
                  b !~ /^\d+\.\s/  &&  # Listas numerada
                  b !~ /^\*\s/     &&  # Listas no numerada
                  b !~ /^-\s/      &&  # Listas no numerada
                  b !~ /^+\s/      &&  # Listas no numerada
                  b !~ /^@type/    &&  # Tipos de lista de Pecas Markdown
                  b !~ /^>/        &&  # Bloques de cita
                  b !~ /^!\[/      &&  # Bloques de imagen
                  b !~ /^```/      &&  # Bloques de código
                  b !~ /^</            # Bloques de HTML
    else
      condition = all
    end

    # Realiza el ajuste
    if condition && !/\\\n/.match(b)
      b = b.gsub("\n", ' ')                 # Todo a una línea
           .gsub(/ +/, ' ')                 # Eliminación de espacios dobles
           .split(/(.{#{chars}}[^ ]*?)\s/)  # División según la cantidad de caracteres establecida
           .join("\n")                      # Unión con saltos de línea
           .gsub(/\n\n/, "\n")              # Eliminación de saltos de línea dobles
           .strip                           # Eliminación de espacios al inicio o al final

      final.push(b)
    else
      final.push(b)
    end
  end

  # Añade el salto de línea necesario para respetar bloques de Markdown
  final = final.map {|b| b == final.first ? b : "\n" + b}

  # Guardado
  file = File.open(ARGV.last, 'w:utf-8')
  file.puts final
  file.close

  puts "Eliminando respaldo '#{bak}', ya no es necesario…"
  FileUtils.rm(File.dirname(ARGV.last) + '/' + bak)

  puts "El archivo ha sido ajustado a #{chars.to_i} caracteres por línea de texto."
rescue
  puts "ERROR: el archivo no pudo ser analizado, revisa su formato o ejecuta: texti --help."
end